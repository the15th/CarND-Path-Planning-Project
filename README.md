# CarND-Path-Planning-Project
Self-Driving Car Engineer Nanodegree Program

C++ path planner able to drive a simulated car in a typical three lane highway with traffic around a circular track (4.32 miles)  without going over the speed limit, keep inside its lane or avoid hitting other neighbour cars. 



##  Reflection on how to generate paths
 In its current version it consists of  the following modules:
* The class Vehicle([vehicle.h](src/vehicle.h)):
  * Used to create an object Vehicle for the ego car (based on main car's localization data, [lines 238-252 in main.cpp](src/main.cpp#L238-L252)) and the other cars on the highway (based on Sensor Fusion Data, [lines 288-302 in main.cpp](src/main.cpp#L288-L302))
  * It wraps the functions(*__choose_next_state__, __successor_states__*, [lines 46-96 in vehicle.cpp](src/vehicle.cpp#L46-L96)) of the behavior planner that decides what subsequent states are possible given any state of the car. The logic of the behaviour planner is based on a simply finite state machine with three states(__keep lane(KL)__, __lane change left(LCL)__, __lane change right(LCR)__) and ([two cost functions](src/cost.cpp)) used to computes collisions with front, left and right vehicles and the cost for lane changing.
* Trajectory Generation using the spline library([spline.h](src/spline.h)) implemented following suggestions of the final lesson walkthrough tutorial (in file [main.cpp, lines 333-438](src/main.cpp#L333-L437)).


### The lane changing decision making process logic 
Here some details on the cost functions and explanations of the intended behaviors:  
* [__checking vehicle ahead in front of ego car:__](src/vehicle.cpp#L37-L115)  returns a true if a vehicle is found ahead of the current vehicle and basically triggers behavior planning algorithm. Also always inside this function, according to the current lane in which the car is present, it is evaluated whether there are cars in the adjacent lanes left and right. The weights that will be used to evaluate lane change in the [change_lane_cost function](src/cost.cpp) are calculated  
* [__collision_cost function:__](src/cost.cpp#L3-L33) checks the gap distance, [set to 30 meters](src/vehicle.h#L10), between the ego car and the other cars, if gap (its modulus) is below a the preassigned gap threshold it returns a penalty cost (a contribute inversely proportional to the gap distance, so bigger for near distances  [lines 21-29 in file cost.cpp](src/cost.cpp#L22-L30)) and inside [__choose_next_state__](src/vehicle.cpp#L46-L67) function the state with the lowest cost will be chosen ([lines 54-63 in vehicle.cpp](src/vehicle.cpp#L54-L63)).
* [__lane change cost:__](src/cost.cpp) weight computation proportional to the number of non-ego cars on the left and right side of the ego car
* __successor_states function([lines 69-96 in file vehicle.cpp](src/vehicle.cpp#L195-L220)):__ provides the possible next states given the current state and lane for the three-state FSM mentioned above.
* __choose_next_state function([lines 46-67 in file vehicle.cpp](src/vehicle.cpp#L117-L193)):__ return the next state among the ones with minum cost. [Other conditions are evaluated](src/cost.cpp#L140-L189) and imposed according to the lane number to avoid unexpected behavior during the simulation by the ego-car    
* __lane switching when it's safe to change([lines 311-330 in main.cpp](src/main.cpp#L311-L330)):__ once a neighbour vehicle near the ego car is detected on the current lane and next state is computed, according to this new state there will be a change only if there are no cars incoming on the new lane(checking next state status and the boolean flag right_lane_detected/left_lane_detected inside the conditional statement)

### Trajectory Generation by using the spline library
The calculation of the trajectory is based on the speed and lane output from the behavior, car coordinates and past path points. The last two points of the previous trajectory, or the car position if there are no previous trajectory[lines 337-369 in main.cpp](src/main.cpp#L337-L369) are used together with [three points](src/main.cpp#L369-L374) which are 30m, 60m, 90m away in s position and new lane d position in frenet coordinates to [initialize the spline calculation](src/main.cpp#L392-L394). To make the work less complicated to the spline calculation based on those points, [the coordinates are transformed (shift and rotation) to local car coordinates](src/main.cpp#L383-L390)

##  Specifications met 
* The car is able to drive at least 4.32 miles without incident and not coming into contact with any of the other cars on the road.
* The car drives according to the speed limit and does not exceed a total acceleration of 10 m/s^2 and a jerk of 10 m/s^3.
* The car is able to change lanes and doesn't spend more than a 3 second length out side the lane lanes during changing step  

It is possible to check what is stated  above in the following video of the car driving without incident more than 9 miles:  
https://youtu.be/q-E5i9Wc0EU 

![path planning](output_images/pathplanning4.png)


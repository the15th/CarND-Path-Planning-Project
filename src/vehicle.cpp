#include "vehicle.h"
#include "cost.h"

  void Vehicle::configure(vector<double> vehicle_data){
    /*
    Called by simulator at each iteration. Sets various
    parameters which will impact the ego vehicle and other non-ego vehicles  in the highway.
    */
    id = (int)vehicle_data[0];
    x  = vehicle_data[1];
    y  = vehicle_data[2];
    vx = vehicle_data[3];
    vy = vehicle_data[4];
    s  = vehicle_data[5];
    d = vehicle_data[6];
    speed = vehicle_data[7];
    yaw = vehicle_data[8];
    lane = vehicle_data[9];

    // if you are not configuring the ego car
    // calculate lane for the other non-ego cars
    if(lane < 0){
      lane = int(d/4.0);
    }
    else{
      state = "KL";
      left_lane_detected = false;
      right_lane_detected = false;
      num_car_left = 0;
      num_car_right = 0;
      num_car_center = 0;
      wl = 0, wr = 0, wc = 0;
    }

  }

  bool Vehicle::get_vehicle_ahead(Vehicle vehicle){
    //returns a true if a vehicle is found ahead of the current vehicle, false otherwise
    bool too_close = false;
    neighbor_vehicles.push_back(vehicle);
    double check_speed = vehicle.speed;
    double check_car_s = vehicle.s;
    double car_s = s;
    double ref_vel = 49.5;
    double gap = 0;
    double gap_speed = 0;
    // Car is in my lane
    if (lane == vehicle.lane) {
      if(fabs(car_s - check_car_s) < TARGETDISTANCE*2){
        num_car_center++;
        // check new car s position
        check_car_s += ((double)prev_size*0.02*check_speed);
        if( (check_car_s > car_s) && (check_car_s - car_s < TARGETDISTANCE) ){
          too_close = true;
          gap = check_car_s - car_s;
          wc += (1.0 - gap/TARGETDISTANCE);
        }
      }
    }
    // leftmost lane
    if(lane == 0){
      //if cars detected on right side
      if ( vehicle.lane - lane == 1 ){

        if(fabs(car_s - check_car_s) < TARGETDISTANCE)
          right_lane_detected = true;
        if(fabs(car_s - check_car_s) < TARGETDISTANCE*2){
          num_car_right++;
          gap = fabs(check_car_s - car_s);
          check_car_s += ((double)prev_size*0.02*check_speed);
          if((check_speed - speed) > 0)
            gap_speed = fabs(check_speed -speed) / ref_vel;
          else gap_speed = 0;
          wr += (1.0 - fabs(gap/(TARGETDISTANCE*2))) + gap_speed;
        }
      }
    }
    // central and rightmost lane
    else if( (lane == 1) || (lane == 2)){
      if ( vehicle.lane - lane == -1 ){

        if (fabs(car_s - check_car_s) <= TARGETDISTANCE)
          left_lane_detected = true;
        if (fabs(car_s - check_car_s) <= TARGETDISTANCE*2){
          num_car_left++;
          double gap = fabs(check_car_s - car_s);
          check_car_s += ((double)prev_size*0.02*check_speed);
          double gap_speed = 0;
          if((check_speed - speed) > 0)
            gap_speed = (check_speed -speed) / ref_vel;
          else gap_speed = 0;
          wl += (1.0 - fabs(gap/(TARGETDISTANCE*2))) + gap_speed;
        }
      }
      // if cars detected on right side
      else if ( vehicle.lane - lane == 1 ){

        if(fabs(car_s - check_car_s) <= TARGETDISTANCE)
          right_lane_detected = true;
        if(fabs(car_s - check_car_s) <= TARGETDISTANCE*2)
        {
          num_car_right++;
          double gap = fabs(check_car_s - car_s);
          check_car_s += ((double)prev_size*0.02*check_speed);
          double gap_speed = 0;
          if((check_speed -speed) > 0)
            gap_speed = (check_speed -speed) / ref_vel;
          else gap_speed = 0;
          wr += (1.0 - fabs(gap/(TARGETDISTANCE*2))) + gap_speed;
        }
      }
    }
    //cout << "wl " << " wr" << " wc " << wl <<" "<< wr << " " << wc << endl;
    return too_close;
  }

  string Vehicle::choose_next_state(){
    vector<string> states = successor_states();
    map<string,double> statesmap;
    if(states.size() == 1)
      return "KL";

    // Find the mininum cost state
    string next_state;
    double mincost = 1e3;
    for (string state : states) {
      double cost = 0;
      cost = collision_cost( lane, state, s, prev_size, neighbor_vehicles)+
          change_lane_cost(state,wl,wr);


      statesmap.insert(make_pair(state, cost));
      if (cost < mincost) {
        mincost = cost;
        next_state = state;
      }
      cout << "Cost state " <<  state << ": " << cost <<  endl;
    }

    if(lane == 0){
      if(statesmap.size() ==  2){
        if(statesmap["LCR"] == statesmap["KL"]){
          if(right_lane_detected)
            next_state = "KL";
        }
      }

    }

    else if(lane == 1){
      if(statesmap.size() == 3){
        if(statesmap["LCL"] == statesmap["LCR"] && statesmap["LCL"] == statesmap["KL"]){
          if(left_lane_detected  && right_lane_detected)
            next_state = "KL";
          else if(left_lane_detected  && !right_lane_detected)
            next_state = "LCR";
          else if(!left_lane_detected  && right_lane_detected)
            next_state = "LCL";
          else {
            if(num_car_left > num_car_right)
             next_state = "LCR";
            else next_state = "LCL";
          }

        }

        else if(statesmap["LCL"] == statesmap["LCR"]  && statesmap["LCL"] < statesmap["KL"]){
          if(right_lane_detected && left_lane_detected)
            next_state = "KL";
          else if(right_lane_detected && !left_lane_detected)
            next_state = "LCL";
          else {
            if(num_car_left > num_car_right)
              next_state = "LCR";
            else next_state = "LCL";
          }

        }
      }

    }
    else if(lane == 2){
      if(statesmap.size()==2){
        if(statesmap["LCL"] == statesmap["KL"]){
          if(left_lane_detected)
            next_state = "KL";
        }
      }
    }

    return next_state;

  }

  vector<string> Vehicle::successor_states() {

    vector<string> states;
    string current = state;
    states.push_back (current);

    if (current.compare("KL") == 0) {

      if (lane == 0 && !right_lane_detected) {
        states.push_back("LCR");
      }

      if (lane == 1 && !right_lane_detected) {
        states.push_back("LCR");
      }
      if (lane == 1 && !left_lane_detected) {
        states.push_back("LCL");
      }

      if (lane == 2 && !left_lane_detected) {
        states.push_back("LCL");
      }

    }
    return states;
  }





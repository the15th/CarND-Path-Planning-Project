#ifndef VEHICLE_H
#define VEHICLE_H
#include <iostream>
#include <random>
#include <vector>
#include <map>
#include <string>
#include <cmath>

#define TARGETDISTANCE 30


using namespace std;

class Vehicle {
public:

  int id;
  double x;
  double y;
  double s;
  double d;
  double yaw;
  double vx;
  double vy;
  double speed;
  int lane;


  int prev_size;
  string state;
  vector<Vehicle> neighbor_vehicles;
  bool left_lane_detected;
  bool right_lane_detected;
  int num_car_left, num_car_right, num_car_center;
  double wl, wr, wc;


  /**
  * Constructor
  */
  Vehicle(){}

  void configure(vector<double> vehicle_data);

  /**
  * Destructor
  */
  ~Vehicle(){}

  string choose_next_state();
  vector<string> successor_states();
  bool get_vehicle_ahead(Vehicle vehicle);




};


#endif /* VEHICLE_H_ */

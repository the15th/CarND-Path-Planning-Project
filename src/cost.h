#ifndef COST_H_
#define COST_H_

#include "vehicle.h"

double change_lane_cost(string state, double wl, double wr);

double collision_cost(int lane, string state, double s, int prev_size,  vector<Vehicle> neighbor_vehicles);
#endif /* COST_H_ */

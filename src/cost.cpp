#include "cost.h"

double collision_cost(int lane, string state, double s, int prev_size,  vector<Vehicle> other_vehicles){
  int lane_to_check;
  if (state.compare("LCL") == 0){
    lane_to_check = lane - 1;
  }
  else if (state.compare("LCR") == 0){
    lane_to_check = lane + 1;
  }
  else lane_to_check = lane;
  double cost = 0;
  for (Vehicle vehicle : other_vehicles) {
    // Car is in my lane
    if (lane_to_check == vehicle.lane) {
      double check_speed = vehicle.speed;
      double check_car_s = vehicle.s;

      // check new car s position
      check_car_s += ((double)prev_size*0.02*check_speed);
      double gap_s = check_car_s - s;
      if (state.compare("KL") == 0){
        if (gap_s > 0 && gap_s < TARGETDISTANCE){
          //cost += (1 - gap_s/TARGETDISTANCE);
          cost += 1/gap_s;
        }
      }else if(fabs(gap_s) < TARGETDISTANCE*2)
        cost += //( 1 - fabs(gap_s/TARGETDISTANCE));
            1/fabs(gap_s);
    }
  }
  return cost;
}

double change_lane_cost(string state, double wl, double wr) {
  if (state.compare("KL") == 0) {
    return 0;
  } else if(state.compare("LCL") == 0) {
    return wl;
  }
  else if(state.compare("LCR") == 0) {
    return wr;
  }
}


